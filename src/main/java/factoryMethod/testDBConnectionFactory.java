/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factoryMethod;

import com.mycompany.flightreservation.view.SearchFlightFrame;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class testDBConnectionFactory {

    public static void main(String[] args) throws SQLException {
        Properties prop = new Properties();
	InputStream input = null;
        String brand = null; 
        String dbServer = null;
        String port= null ; 
        String dbName = null;
        String userName = null;
        String password = null; 
        String dbTable = null;
	try {
		input = new FileInputStream("src//main//resources//DB.properties");

		// load a properties file
		prop.load(input);

		// get the property value and print it out
		brand = prop.getProperty("BRAND");
                dbServer = prop.getProperty("Domain_Name");
                port = prop.getProperty("PORT_Number");
                dbName = prop.getProperty("DB_NAME");
                userName = prop.getProperty("User_Name");
                password = prop.getProperty("Password");
                dbTable = prop.getProperty("DB_TABLE");
                
                System.out.println("brand:" + brand);
                System.out.println("host:" + dbServer);
                System.out.println("port:" + port);
                System.out.println("DB name:" + dbName);
                System.out.println("user name:" + userName);
                System.out.println("password:" + password);
                System.out.println("DB table::" + dbTable);
                
		
                
                
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SearchFlightFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SearchFlightFrame.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        
        
        
        
        DBConnectionFactory factory = new DBConnectionFactory();
        DBConnection connection
                = factory.createDBConnection(brand, dbServer, port, dbName, userName, password);
        if (connection != null) {
            printTable(connection);
            connection.update("UPDATE LoginAccount SET password='1234' WHERE userName='nui' ;");
            printTable(connection);
            
            connection.close();
        }

    }

    private static void printTable(DBConnection connection) throws SQLException {
        ResultSet results = connection.executeQuery("select * from LoginAccount;");
        while (results.next()) {
            System.out.println("User Name:" + results.getString(1));
            System.out.println("Password:" + results.getString(2));
        }
    }
}
