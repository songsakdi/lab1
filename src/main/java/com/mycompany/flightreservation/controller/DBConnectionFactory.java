/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.flightreservation.controller;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S. Rongviriyapanish
 */
// Factor or Creator
public class DBConnectionFactory {
    public Connection getDBConnection(String brand, String user, String password, String url){
        try {
            MySQLConnectionFactory factory = new MySQLConnectionFactory(user, password, url);
            if (brand.equalsIgnoreCase("mysql"))
                return factory.createConnection();
            else {
                return null ;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
            return null ;
        }
    }
}
