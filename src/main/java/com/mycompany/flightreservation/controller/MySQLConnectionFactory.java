/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.flightreservation.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author S. Rongviriyapanish
 */
// Abstract Product 
public class MySQLConnectionFactory  {
    String url ;
    String user ;
    String password ;
    Connection conn ;
    
    public MySQLConnectionFactory(String user, String pwd, String url){
        this.user = user ;
        this.password = pwd ;
        this.url = url ;
    }
    public void openConnection(String user, String pwd, String url){
    }
    
    public void closeConnection(){
    }
    
    public ResultSet executeStatement(String sqlCommand){
        return null ;
    }
    
    public Connection createConnection() throws SQLException{
        conn = DriverManager.getConnection(url, user, password);
        return conn ;
    }
}
