/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;

import com.mycompany.flightreservation.domain.Flight;
import java.util.List;


/**
 *
 * @author User
 */
public interface IFlightManagement {
    List<Flight> searchFlight() throws MissingRequiredTripInfoException ;
}
